package example

import (
	"log"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
)

func resourceExample() *schema.Resource {
	log.Printf("[DEBUG] In declaration.")
	return &schema.Resource{
		Create: resourceExampleCreate,
		Read:   resourceExampleRead,
		Delete: resourceExampleDelete,

		Schema: map[string]*schema.Schema{
			"cluster_name": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "ID or name of the OpenShift cluster",
				ForceNew:    true,
			},
			"openshift_installer_path": {
				Type:     schema.TypeString,
				Optional: true,
				Default:  "openshift-install",
				ForceNew: true,
			},
			"working_dir": {
				Type:     schema.TypeString,
				Optional: true,
				ForceNew: true,
			},
			"pull_secret": {
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"platform": {
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"base_domain": {
				Type:     schema.TypeString,
				Optional: true,
				ForceNew: true,
			},
			"region": {
				Type:     schema.TypeString,
				Optional: true,
				ForceNew: true,
			},
		},
	}
}

func resourceExampleCreate(d *schema.ResourceData, meta interface{}) error {
	return nil
}

func resourceExampleRead(d *schema.ResourceData, meta interface{}) error {
	return nil
}

func resourceExampleDelete(d *schema.ResourceData, meta interface{}) error {
	return nil
}
