package main

import (
	"log"

	"github.com/hashicorp/terraform-plugin-sdk/plugin"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"gitlab.com/chunks1016.coder/golang-bazel-example/example"
)

func main() {
	log.Printf("[DEBUG] In openshift provider")
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: func() terraform.ResourceProvider {
			return example.Provider()
		},
	})
}
